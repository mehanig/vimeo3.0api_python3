__author__ = 'mehan_000'
import json

from vimeo import VimeoClient
vimeo = VimeoClient("3d3e75a84b7272f669cf378e70bc3f18")


#Constant used for maximum videos per request
SET_MAX_LIMIT = 500

#function used to json objects become readable by humans
def printer(json_obj):
    return json.dumps(json_obj, sort_keys=True, indent=4, separators=(',', ': '))


if __name__ == "__main__":

    # vimeo.me.videos - this method returns JSON obj containing info about all your videos
    raw_data = json.dumps(vimeo.me.videos(per_page=SET_MAX_LIMIT))
    data = json.loads(raw_data)
    inp = "q"
    for i, video in enumerate(data['body']['data']):
        print(" ".join(("Video Number:", str(i), "--->", str(printer(video['name'])))))
    #print(printer(data))


    #UPLOADER
    #vimeo.upload("A_D.mp4")

    while True:
        try:
            inp = (input('Q to exit. Enter number of Video:'))
            video_num = int(inp)
            for i, links in enumerate(data['body']['data'][video_num]['files']):
                print(" ".join(("Link to",printer(links['quality']), printer(links['link_secure']),
                                "Size:", printer(links['width']), "x", printer(links['height']))))
                if 2 == i:
                    break
        except ValueError:
            print("Not a number")
            if "q" == inp:
                break

